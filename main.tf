module "Griffin_Dev" {
  source = "./NetworkandSubnets"
  vpc_name = "griffin-dev-vpc"
  subnet_1_name = "griffin-dev-wp"
  subnet_2_name = "griffin-dev-mgmt"
  subnet_1_cidr = "192.168.16.0/20" 
  subnet_2_cidr = "192.168.32.0/20"
  firewall_name = "sshdev"
}
module "Griffin_Prod" {
  source = "./NetworkandSubnets"
  vpc_name = "griffin-prod-vpc"
  subnet_1_name = "griffin-prod-wp"
  subnet_2_name = "griffin-prod-mgmt"
  subnet_1_cidr = "192.168.48.0/20" 
  subnet_2_cidr = "192.168.64.0/20"
  firewall_name = "sshprod"
}

resource "google_compute_instance" "bastion" {
  name = "bastion"  
  zone = "us-east1-b"
  machine_type = "n1-standard-1"
  network_interface {
    network = module.Griffin_Dev.vpc_id
    subnetwork = module.Griffin_Dev.subnet_2_id
  }
  network_interface {
    network = module.Griffin_Prod.vpc_id
    subnetwork = module.Griffin_Prod.subnet_2_id
  }
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }
}