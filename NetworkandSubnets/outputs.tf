output "vpc_id"{
  value = google_compute_network.vpc.id
}
output "subnet_1_id"{
  value = google_compute_subnetwork.subnet_1.id
}
output "subnet_2_id" {
  value = google_compute_subnetwork.subnet_2.id 
}