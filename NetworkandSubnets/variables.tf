variable "vpc_name" {
  type = string
}
variable subnet_1_name { 
  type = string
}
variable "subnet_2_name" {
  type = string 
}
variable "subnet_1_cidr" {
  type = string
}
variable "subnet_2_cidr" {
  type = string
}
variable "firewall_name" {
  type = string
}
variable "region" {
  type = string
  default = "us-east1"
}