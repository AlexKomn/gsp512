resource "google_compute_network" "vpc" {
  name =  var.vpc_name
  auto_create_subnetworks = false
  mtu = 1460
}

resource "google_compute_subnetwork" "subnet_1" {
  name = var.subnet_1_name
  network = google_compute_network.vpc.id
  ip_cidr_range = var.subnet_1_cidr
  region = var.region
}

resource "google_compute_subnetwork" "subnet_2" {
  name = var.subnet_2_name
  network = google_compute_network.vpc.id
  ip_cidr_range = var.subnet_2_cidr
  region = var.region
}

resource "google_compute_firewall" "ssh-icmp" {
  name = var.firewall_name
  network = google_compute_network.vpc.id

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
  source_ranges = [ "0.0.0.0/0" ]
}